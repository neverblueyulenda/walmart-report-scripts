#!/usr/bin/env python

import os, getopt, sys, datetime, json, re, yaml

class ParseApiResults():

    def split_u1(self, u1):
        valid_u1_format = re.compile(r"(?P<oid>\d+)__(?P<aid>\d+)__(?P<reqid>\d+)")
        return valid_u1_format.match(u1)
        
        
    def split_u1_lite(self, u1):
        valid_u1_format = re.compile(r"(?P<oid>\d+)__(?P<aid>\d+)__(?P<reqid>[\w\d(\s+/=)*(\-\_)*]+)")
        return valid_u1_format.match(u1)      
    
    def usage(self):
            print """
-h --help
-d --debug
-n --nooutput  if set, no .json file will be created
-f --filename=apiresults_YYYY-MM-DD_HH.json (json filename to parse)
""" 
    def run(self):
        # get cmd line stuff
        self.nooutput = False
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                "hdfn",
                ["help", "debug", "filename=", "nooutput"])
        except getopt.GetoptError, err:
            print str(err)
            sys.exit(2)
        for o, a in opts:
            if o in ("-d", "--debug"):
                self.debug = True
            elif o in ("-n", "--nooutput"):
                self.nooutput = True
            elif o in ("-h", "--help"):
                self.usage() 
                sys.exit()
            elif o in ("-f", "--filename"):
                self.filename = a
            else:
                assert False, "unhandled option"
                
        if not self.filename:
            print str("File to parse required")
            sys.exit(2)
        
        try:
            report_json_data = open(self.filename)
            #self.report_results = json.load(report_json_data)
            self.report_results = yaml.load(report_json_data)
            #print "{0}".format(self.report_results)
            #sys.exit(2)
        except Exception, err:
            print str(err)
            sys.exit(2)
            
    
    def main(self):
        total_gmv = 0
        total_gmv_by_offer = {}
        total_orders = 0
        total_by_aff = {}
        total_by_offer_aff = {}
        old_u1 = 0
        new_u1 = 0
        
        for u1 in self.report_results:
            #print "processing u1={0}".format(u1["u1"])
            total_orders += len(u1["orders"])
            u1_parts = self.split_u1(u1["u1"])
            if not u1_parts:
                # try checking against lite clicks
                u1_parts = self.split_u1_lite(u1["u1"])
                if not u1_parts:
                    old_u1 += 1
                    #last_error = "Walmart u1={0} parameter not in expected format"\
                    #    " of OFFERID__AFFILIATEID__REQID".format(u1["u1"])
                    #print(last_error)
                    # still add to counts
                    affiliate = u1["u1"]
                    temp_aff_gmv = 0
                    temp_aff_orderid = []
                    for order in u1["orders"]:
                        total_gmv += order["amount"]
                        temp_aff_gmv += order["amount"]
                        temp_aff_orderid.append(order["id"])
                        
                    if affiliate not in total_by_aff:
                        total_by_aff[affiliate] = {
                            "gmv": temp_aff_gmv,
                            "orderids": temp_aff_orderid,
                            "reqids": [],
                            "offers": []
                        }
                    else:
                        total_by_aff[affiliate]["gmv"] += temp_aff_gmv
                        total_by_aff[affiliate]["orderids"] += temp_aff_orderid
                    # go to next u1 parameter, maybe it has correct format
                    continue
            new_u1 += 1
            offer = u1_parts.group("oid")
            affiliate = u1_parts.group("aid")
            reqid = u1_parts.group("reqid")
            temp_aff_gmv = 0
            temp_aff_gmv_by_offer = {}
            temp_aff_orderid = []
            temp_aff_orderid_by_offer = {}
            for order in u1["orders"]:
                total_gmv += order["amount"]
                if offer in total_gmv_by_offer:
                    total_gmv_by_offer[offer] += order["amount"]
                else:
                    total_gmv_by_offer[offer] = order["amount"]
                temp_aff_gmv += order["amount"]
                temp_aff_orderid.append(order["id"])
                if offer in temp_aff_gmv_by_offer:
                    temp_aff_gmv_by_offer[offer] += order["amount"]
                else:
                    temp_aff_gmv_by_offer[offer] = order["amount"]
                if offer in temp_aff_orderid_by_offer:
                    temp_aff_orderid_by_offer[offer].append(order["id"])
                else:
                    temp_aff_orderid_by_offer[offer] = [order["id"]]
                    
            if affiliate not in total_by_aff:
                total_by_aff[affiliate] = {
                    "gmv": temp_aff_gmv,
                    "orderids": temp_aff_orderid,
                    "reqids": [reqid],
                    "offers": [offer]
                }
                #print "{0}".format(total_by_aff)
            else:
                total_by_aff[affiliate]["gmv"] += temp_aff_gmv
                total_by_aff[affiliate]["orderids"] += temp_aff_orderid
                total_by_aff[affiliate]["reqids"].append(reqid)
                if offer not in total_by_aff[affiliate]["offers"]:
                    total_by_aff[affiliate]["offers"].append(offer)
                    
            
            if offer not in total_by_offer_aff:
                total_by_offer_aff[offer] = {
                    'gmv': temp_aff_gmv_by_offer[offer],
                    'total_orders': len(temp_aff_orderid_by_offer[offer]),
                    'affiliates': {}
                }
            else:
                total_by_offer_aff[offer]['gmv'] += temp_aff_gmv_by_offer[offer]
                total_by_offer_aff[offer]['total_orders'] += len(temp_aff_orderid_by_offer[offer])
                
            if affiliate not in total_by_offer_aff[offer]['affiliates']:
                total_by_offer_aff[offer]['affiliates'][affiliate] = {
                    "gmv": temp_aff_gmv_by_offer[offer],
                    "orderids": temp_aff_orderid_by_offer[offer],
                    "reqids": [reqid]
                }
            else:
                total_by_offer_aff[offer]['affiliates'][affiliate]["gmv"] += temp_aff_gmv_by_offer[offer]
                total_by_offer_aff[offer]['affiliates'][affiliate]["orderids"] += temp_aff_orderid_by_offer[offer]
                total_by_offer_aff[offer]['affiliates'][affiliate]["reqids"].append(reqid)
                

        print "From file={0}, total_gmv={1}, total_order={2} "\
            "old_u1={3} new_u1={4}".format(
            self.filename, total_gmv, total_orders, old_u1,
            new_u1)        
        
        """
        print "\naffiliate\tgmv\ttotal_orders"
        for aff in sorted(total_by_aff):
            #print "{0}".format(aff)
            if len(aff) < 6:
                #print "\taffiliate={0}\t\tgmv={1}\ttotal_orders={2}\ttotal_reqids={3}".format(
                print "{0}\t{1}\t{2}".format(
                    aff, total_by_aff[aff]["gmv"], 
                    len(total_by_aff[aff]["orderids"])
                    #,len(total_by_aff[aff]["reqids"])
                )
            else:
                #print "\taffiliate={0}\tgmv={1}\ttotal_orders={2}\ttotal_reqids={3}".format(
                print "{0}\t{1}\t{2}".format(
                    aff, total_by_aff[aff]["gmv"], 
                    len(total_by_aff[aff]["orderids"])
                    #,len(total_by_aff[aff]["reqids"])
                )
        """
                
        for offer in total_by_offer_aff:
            print '-------------------------'
            print 'offer={0}\ttotal_orders={1}\ttotal_gmv={2}\n'.format(
                offer, total_by_offer_aff[offer]['total_orders'],
                total_by_offer_aff[offer]['gmv']
            )
            print '\taffiliate\ttotal_orders\tgmv'
            for aff in total_by_offer_aff[offer]['affiliates']:
                aff_details = total_by_offer_aff[offer]['affiliates'][aff]
                print '\t{0}\t\t{1}\t\t{2}'.format(aff,
                    len(aff_details['orderids']),aff_details['gmv'])

               
        if not self.nooutput:
            tempfile = self.filename.split('/')
            tmpfile = tempfile[len(tempfile)-1].split('.')
            filetoopen = '/home/mobconv/walmart-api/parseddata/' + tmpfile[0] + '_sum_by_affiliate.json'
            print 'file to write sum by affiliate to={0}'.format(filetoopen)
            f = open(filetoopen, 'w')
            strtowrite = '{0}'.format(total_by_aff)
            f.write(strtowrite.replace("'",'"'))
            f.close()
        
        print '============================================================'
        

if __name__ == "__main__":
    r = ParseApiResults()
    r.run()
    r.main()          
