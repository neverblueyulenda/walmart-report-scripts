#!/usr/bin/env python

import datetime, requests
import getopt, sys, os

try:
    opts, args = getopt.getopt(sys.argv[1:], "hfd", ["help", "debug", "date="])
except getopt.GetoptError, err:
    print '{0}'.format(err)    

specified_date = False
debug = False

for o, a in opts:
    if o in ('-f', '--date'):
        specified_date = a.lower()
    elif o in ('-d', '--debug'):
        debug = True
    elif o in ('h', '--help'):
        print """
-h --help
-d --debug
-f --date=date(YYYY-MM-DD) or --date=yesterday if no date specified, today will be used
"""
        sys.exit()
    else:
        assert False, "unhandled option"

if specified_date == 'yesterday':
    reportdate = datetime.datetime.utcnow() - datetime.timedelta(1)
elif specified_date:
    try:
        reportdate = datetime.datetime.strptime(specified_date, '%Y-%m-%d')
    except Exception, err:
        print '{0}'.format(err)
else:    
    reportdate = datetime.datetime.today()
    
epoch = datetime.datetime.utcfromtimestamp(0)
reldate = reportdate - epoch
#now = datetime.datetime.today()
now = datetime.datetime.utcnow()

if debug:
    print "Getting Walmart report for date={0}_{1}".format(reportdate.strftime('%Y-%m-%d'), now.hour)

filetoopen = '/home/mobconv/walmart-api/api-reports/%s_apiresults_%s.json' % (reportdate.strftime('%Y-%m-%d'), reldate.days)
try:
    # check file contents in case of failure
    with open(filetoopen, 'r') as report:
        contents = report.read().replace('\n', '')
        if 'Failed Walmart API' in contents or contents == '{}':
            print "{0} already exists, but failed to get real data, try again".format(filetoopen)
        else:
            print "{0} already exists, and was successful, no need to fetch again".format(filetoopen)
            sys.exit(1)
except Exception, err:
    print "Fetching API report {0}".format(filetoopen)

f = open(filetoopen, 'w')
#linkshare account
#walmart_url = 'http://api.walmartlabs.com/v1/reports/realtime/aggregated?apiKey=585fe8c5shnan9hhs2jrtgz4&date=%s' % reldate.days
#direct walmart account
walmart_url = 'http://api.walmartlabs.com/v1/reports/realtime/aggregated?apiKey=37hrq8ahuzpmzn3k5fzhqpzn&date=%s' % reldate.days

if debug:
    print "calling walmart_url={0}".format(walmart_url)

r = requests.get(walmart_url)

if debug:
    print "API result: status_code={0}".format(r.status_code)    

if r.status_code != requests.codes.ok:
    # write error
    if debug:
        print "Failed result, error={0}".format(r.text)
    f.write('Failed Walmart API request={0} - returned status_code={1} '
        'response={2} report_date={3}'.format(walmart_url, r.status_code, 
        r.text, reportdate.strftime('%Y-%m-%d')))
else:
    formattedtext = '{0}'.format(r.text)
    f.write(formattedtext)
    if debug:
        print "Saving results to: {0}".format(filetoopen)

f.close()

