#!/usr/bin/env python

import getopt, sys, re, csv, time, os, requests

pidfile = "/tmp/parsewalmartftpfile.pid"

class ParseFTPReportResults():
    def exitscript(self, msg, exitcode=1):
        print msg
        os.unlink(pidfile)
        sys.exit(exitcode)


    def split_u1(self, u1):
        valid_u1_format = re.compile(r"(?P<oid>\d+)__(?P<aid>\d+)__(?P<reqid>\d+)")
        return valid_u1_format.match(u1)
        
        
    def split_u1_lite(self, u1):
        valid_u1_format = re.compile(r"(?P<oid>\d+)__(?P<aid>\d+)__(?P<reqid>[\w\d(\s+/=)*(\-\_)*]+)")
        return valid_u1_format.match(u1)


    def usage(self):
            print """
-h --help
[REQUIRED] --reportlocation=/absolute/path/to/report/location/
        eg: /home/mobconv/ftpfiles/walmart/ - include the trailing slash /

[OPTIONAL] --filename=/home/mobconv/ftpfiles/walmart/Walmart_Orders_YYYYMMDD.csv (CSV filename to parse)
  	   if set, please specify the absolute path
	   if not set, will default to the report with today's date found in the reportlocation folder
""" 
    def run(self):
        # get cmd line stuff
        self.reportlocation = False
        self.filename = False
        # list of transactionIds processed from the API on 2015-12-07 and 2015-12-08
        self.processed_transactions = ['6131522391861', '6131523000298', '6131522486493', '6131523514145', '6131523413676', '6131523515423', '6131523728924', '6131522885803', '6131523201033', '6131523421555', '6131523229019', '6131522385329', '6131523627285', '6131522683966', '6131522794924', '6131522089652', '6131523500219', '6131523601741', '6131522492601', '6131522398954', '6131522891198', '6131522585411', '6131523322857', '6131523113214', '6131523813898', '6131522089389', '6131522584003', '6131523613828', '6131522686453', '6131522485611', '6131523601969', '6131523917605', '6131523001428', '6131523001202', '6131522398584', '6131522391022', '6131522499401', '6131523914098', '6131523329480', '6131523727925', '6131522290170', '6131522283676', '6131523601501', '6131522092772', '6131523219440', '6131523817755', '6131522186969', '6131522398399', '6131523430404', '6131522590545', '6131522385480', '6131522892019', '6131523118377', '6131522893748', '6131522092842', '6131522485628', '6131522987291', '6131522884823', '6121522148956', '6121522143109', '6121522648135', '6121522932462', '6121522934962', '6131522364955', '6131522261981', '6131522654827', '6131522463379', '6121522750390', '6131522953363', '6121522127301', '6121522124431', '6121522825713', '6131522255412', '6121522336265', '6131522553352', '6121522940398', '6121522134863', '6121522927630', '6131522653281', '6121522535631', '6121522044834', '6121522737977', '6121522929342', '6121522239654', '6121522040948', '6121522047537', '6121522551716', '6121522838147', '6121522440201', '6121522534666', '6121522432374', '6121522943862', '6121522845327', '6121522641299', '6121522535502', '6121522049965', '6121522835577', '6121522831652', '6121522226303', '6121522846792', '6121522727293', '6121522231078', '6121522037516', '6121522340213', '6121522327721', '6121522647365', '6121522441924', '6121522629316', '6121522329329', '6121522630896', '6121522838743', '6121522745808', '6121522835302', '6121522136375', '6121522643164', '6121522333975', '2677030522638', '6121522937066', '6121522336545', '6121522526282', '6121522843591', '6121522433455', '6121522951315', '6121522844356', '6121522747589', '6121522744356', '6121522034712', '6121522441359', '6121522524364', '6121522133828', '6121522528154', '6121522037724', '6121522828938', '6121522545780', '6121522941410', '6121522736538', '6121522947397', '6121522234418', '6121522937335', '6121522736879', '6121522038889', '6121522125494', '6121522543415', '6121522532548', '6121522423689', '6121522827459', '6121522330004', '6121522836999', '6121522441049', '6121522945935', '6121522333785', '6121522647352', '6121522050942', '6121522926017', '6121522832484', '6121522237984', '6121522128400', '6121522239496', '6121522241638', '6121522440030', '6121522039289', '6121522526453', '6121522325996', '6121522835077', '6121522147373', '6121522245350', '6121522531431', '6121522124433', '6121522134457', '6121522542251', '6121522350753', '6121522629283', '6121522135302', '6121522839328', '6121522125961', '6121522134323', '6121522149821', '6121522144920', '6121522732063', '6121522531643', '6121522441672', '6121522840263', '6121522335923', '6121522235356', '6121522914739', '6121522335402', '6121522814673', '6121522927170', '6121522825203', '6121522027342', '6121522506153', '6121522307215', '6121522223977', '6121522808125', '6121522209114', '6121522101931', '6121522112823', '6121522217822', '6121522112087', '6121522701186', '6121522209896', '6121522602580', '6121522408489', '6121522723824', '6121521296874', '6121521799822', '6121522806040', '6121522016508', '6121522214118', '6121522210070', '6121522321123', '6121522903995', '6121522921986', '6121522123119', '6121522714431', '6121522111441', '6121522719300', '6121522815620', '6121522609524', '6121521099934', '6121522217734', '6121522010615', '6121522512673', '6121522213812', '6121522319411', '6121522311239', '6121522503920', '6121522613656', '6121522118862', '6121522801944', '6121521698466', '6121522822510', '6121522013749', '6121522618506', '6121522615062', '6121522723106', '6121522921399', '6121522905401', '6121522817616', '6121522401659', '6121522911242', '6121521399517', '6121522109556', '6121521598872', '6121522814807', '6121522022635', '6121521598142', '6121522921081', '6121522518011', '6121522801409', '6121522702214', '6121521997179', '6121521997950', '6121522021547', '6121522504905', '6121522504104', '6121522804067', '6121522421810', '6121522022882', '6121522405689', '6121522415819', '6121521295785', '6121521297874', '6121521297603', '6121522723829', '6121522004504', '6121522910780', '6121521496896', '6121522023748', '6121522613342', '6121522522642', '6121522109131', '6121522608005', '6121522710700', '6121521797103', '6121522809973', '6121522212897', '6121522506662', '6121522621377', '6121522321187', '6121522018194', '6121522103778', '6121522018360', '6121522619459', '6121522200090', '6121522800738', '6121522418389', '6121522701331', '6121522113816', '6121521996885', '6121522905540', '6121522208734', '6121522806269', '6121522020798', '6121521774873', '6121521884335']

        try:
            opts, args = getopt.getopt(sys.argv[1:],
                "h", ["help", "filename=", "reportlocation="])
        except getopt.GetoptError, err:
            self.exitscript(str(err), 2)
        for o, a in opts:
            if o in ("-d", "--debug"):
                self.debug = True
            elif o in ("-h", "--help"):
                self.usage() 
                self.exitscript('', 0)
            elif o in ("--reportlocation"):
                self.reportlocation = a
            elif o in ("-f", "--filename"):
                self.filename = a
            else:
                assert False, "unhandled option"

        if not self.reportlocation:
            self.exitscript('reportlocation not specified')
        else:
            if not os.path.isdir(self.reportlocation):
                self.exitscript('{0} directory does not exist'.format(self.reportlocation))

        os.environ['TZ'] = 'US/Pacific'
        time.tzset()
        today = time.strftime('%Y%m%d')
        now = time.strftime('%Y-%m-%d %H:%M')
        
        if not self.filename:
            self.inputfile = '{0}Walmart_Orders_{1}.csv'.format(self.reportlocation, today)
            self.outputfile = '{0}Walmart_Orders_{1}_sum_by_affiliate.csv'.format(self.reportlocation, today)
        else:
            self.inputfile = '{0}'.format(self.filename)
            tempfile = self.filename.split('/')
            tmpfile = tempfile[len(tempfile)-1].split('.')
            outputfilename = tmpfile[0]+'_sum_by_affiliate.csv'
            self.outputfile = '{0}{1}'.format(self.reportlocation, outputfilename)
        
        print '==============================='
        # check if input file exists
        try:
            report = open(self.inputfile, 'r')
        except Exception, err:
            self.exitscript('{0} - FTP file: {1} does not exist.'.format(now, self.inputfile))
        
        # check if output file exists, if yes, then don't run again
        try:
            report = open(self.outputfile, 'r')
            self.exitscript('{0} - {1} already exists, no need to run again'.format(now, self.outputfile))
        except Exception, err:
            print '{0} - Processing: {1}...'.format(now, self.inputfile)
    
    def main(self):        
        total_gmv = 0
        total_gmv_by_offer = {}
        total_by_offer_aff = {}
        unique_orderids = []

        rownum = 0
        base_ec_url = 'http://cat2.pulseclick.com/walmart?'
        #base_ec_url = 'http://cat2-stage.pulseclick.com/walmart?'
        
        with open(self.inputfile, 'r') as csvfile:
            for row in csv.reader(csvfile):
                rownum += 1
                if not row:
                    # skipping empty lines
                    continue
            
                if (len(row) is not 11):
                    raise Exception("Invalid row={0} at line={1} in filename={2}, "\
                        "expecting 11 columns".format(row, rownum, self.filename))
                
                if rownum is 1 and \
                    row[0].lower() == 'click_url' and \
                    row[1].lower() == 'click_timestamp' and \
                    row[2].lower() == 'walmart_item_id' and \
                    row[3].lower() == 'offer_id' and \
                    row[4].lower() == 'transaction_type' and \
                    row[5].lower() == 'order_date' and \
                    row[6].lower() == 'quantity' and \
                    row[7].lower() == 'total_revenue' and \
                    row[8].lower() == 'cpa' and \
                    row[9].lower() == 'commission' and \
                    row[10].lower() == 'transaction_date':
                    # this is the header row, ignore it
                    continue
                
                """
                for each row, get the u1 value from the click_url column;
                parse it;
                then sum values by of only auth transactions:
                    [offerid][affiliateid][order_id] = total_revenue
                """
                transaction_type = row[4].strip()
                if transaction_type != 'auth':
                    continue            
                
                orderid = row[3].strip()
                if orderid not in unique_orderids:
                    unique_orderids.append(orderid)
                
                click_url = row[0].strip()
                u1_value = re.search('(?<=u1=)\w+', click_url)
                if not u1_value:
                    print 'u1 not found in {0}'.format(click_url)
                    continue
                u1_parts = self.split_u1(u1_value.group(0))
                if not u1_parts:
                    continue
                
                offer = u1_parts.group("oid")
                affiliate = u1_parts.group("aid")
                reqid = u1_parts.group("reqid")
                
                orderdate = row[5]
                
                gmv = float(row[7].strip())
                total_gmv += gmv
                if offer in total_gmv_by_offer:
                    total_gmv_by_offer[offer]['gmv'] += gmv
                    if orderid not in total_gmv_by_offer[offer]['orderids']:
                        total_gmv_by_offer[offer]['orderids'].append(orderid)
                else:
                    total_gmv_by_offer[offer]= {'gmv': gmv, 'orderids': [orderid]}

                # add to total_by_offer_aff
                if offer in total_by_offer_aff:
                    if affiliate in total_by_offer_aff[offer]['affiliates']:
                        if orderid in total_by_offer_aff[offer]['affiliates'][affiliate]['orders']:
                            total_by_offer_aff[offer]['affiliates'][affiliate]['orders'][orderid]['gmv'] += gmv
                        else:
                            total_by_offer_aff[offer]['affiliates'][affiliate]['orders'][orderid] = {'gmv': gmv, 'order_date': orderdate, 'reqid': reqid}
                        total_by_offer_aff[offer]['affiliates'][affiliate]['total_gmv'] += gmv
                    else:
                        total_by_offer_aff[offer]['affiliates'][affiliate] = {'orders': {orderid: {'gmv': gmv, 'order_date': orderdate, 'reqid': reqid}}, 'total_gmv': gmv}
                else:
                    total_by_offer_aff[offer] = {'affiliates': {affiliate: {'orders': {orderid: {'gmv': gmv, 'order_date': orderdate, 'reqid': reqid}}, 'total_gmv': gmv}}}
        
        print 'total_gmv={1} total_orders={2}'.format(self.filename, total_gmv, len(unique_orderids))
        
        send_count = 0
        success_send = 0
        failed_send = 0
        csvoutput = 'offer, affiliate, reqid, orderid, order_date, order_amount, Sent to EC status, Failed EC postback\n'
        for offer in total_by_offer_aff:
            print '-------------------------'
            print 'offer={0}\ttotal_orders={1}\ttotal_gmv={2}'.format(
                offer, len(total_gmv_by_offer[offer]['orderids']), 
                total_gmv_by_offer[offer]['gmv'])
            print '\n\taffiliate\ttotal_orders\tgmv'
            for aff in total_by_offer_aff[offer]['affiliates']:
                aff_details = total_by_offer_aff[offer]['affiliates'][aff]
                print '\t{0}\t\t{1}\t\t{2}'.format(aff,
                    len(aff_details['orders']),aff_details['total_gmv'])
                for order in aff_details['orders']:
                    order_details = aff_details['orders'][order]

                    if order in self.processed_transactions:
                        status = 'Already processed from API'
                    else:
                        # create EC postback url
                        send_count += 1
                        clickid = '{0}__{1}'.format(order_details['reqid'],offer)
                        url = '{0}clickid={1}&a={2}&orderid={3}&order_amt={4}'.format(
                            base_ec_url, clickid, aff,order,order_details['gmv'])
                        # send to EC
                        r = requests.get(url)
                        if r.status_code != requests.codes.ok:
                            failed_send += 1
                            status = 'FAILED: response_code={0}, {1}'.format(r.status_code, url)
                        else:
                            if r.text != '1':
                                failed_send += 1
                                status = 'FAILED: response_msg={0}, {1}'.format(r.text, url)
                            else:
                                success_send += 1
                                status = 'Successfully sent to EC for processing'

                    csvoutput += '{0}, {1}, {2}, {3}, {4}, {5}, {6}\n'.format(
                        offer, aff, order_details['reqid'], order, 
                        order_details['order_date'], order_details['gmv'],
                        status)
                
        #print 'file to write sum by affiliate to={0}'.format(self.outputfile)
        f = open(self.outputfile, 'w')            
        f.write(csvoutput)
        f.close()
        print '-------------------------'
        print 'Total postbacks sent to EC={0} success={1} failed={2}'.format(
            send_count, success_send, failed_send)
        

if __name__ == "__main__":
    # create pid and check if already exists/running
    pid = str(os.getpid())
    if os.path.isfile(pidfile):
        sys.exit(1)
    else:
        file(pidfile, 'w').write(pid)

    r = ParseFTPReportResults()
    r.run()
    r.main()          

    # clear the pid file
    os.unlink(pidfile)
